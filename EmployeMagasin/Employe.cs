﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeMagasin
{
    class Employe
    {
        private int _numero;
        private string _nom;
        private string _prenom;

        public int numero
        {
            get { return this._numero; }
            set { this._numero = numero; }
        }

        public string nom
        {
            get { return this._nom; }
            set { this._nom = nom; }
        }

        public string prenom
        {
            get { return this._prenom; }
            set { this._prenom = prenom; }
        }

        public Employe(int numeroEmp, string nomEmp, string prenomEmp)
        {
            _numero = numeroEmp;
            _nom = nomEmp;
            _prenom = prenomEmp;
        }

        
    }
}
