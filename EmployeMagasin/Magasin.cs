﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeMagasin
{
    class Magasin
    {
        private string _nomMagasin;
        private List<Employe> _employe;

        public string nomMagasin
        {
            get { return this._nomMagasin; }            
        }

        public List<Employe> employe 
        {
            get { return this._employe; }           
        }

        public Magasin (string nomMagasin)
        {
            _nomMagasin = nomMagasin;
            _employe = new List<Employe>();
        }

        public void AjouterEmployer (Employe unEmploye)
        {
            this._employe.Add(unEmploye);
        }

        public void SupprimerEmployer(int numEmp)
        {
            foreach (Employe employeCourant in _employe)
            {
                if (employeCourant.numero == numEmp)
                {
                    this._employe.Remove(employeCourant);
                }
            }
        } 
        
               
         
        
            
       
    }
}
