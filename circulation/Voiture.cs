﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace circulation
{
    class Voiture
    {
        private float _vitesseMaxi;
        private float _vitesse;
        private bool _demarrer;
        private string _modele;
        private string _marque;

        public Voiture(string aMarque, string aModele, float aVitesseMaxi)
        {
            _marque = aMarque;
            _modele = aModele;
            _vitesseMaxi = aVitesseMaxi;
        }
        public void accelerer(float vitesseEnPlus)
        {
            if (this._demarrer)
            {
                if (this._vitesseMaxi < this._vitesse + vitesseEnPlus)
                {
                    this._vitesse = this._vitesseMaxi;
                }
                else
                {
                    this._vitesse += vitesseEnPlus;
                }
            }
        }
        public void decelerer(float vitesseEnMoins)
        {
            if (this._vitesseMaxi - vitesseEnMoins < 0)
            {
                this._vitesse = 0;
            }
            else
            {
                this._vitesse -= vitesseEnMoins;
            }
        }
        public void VoiturePeugeot(Voiture uneVoiture)
        { }
            public void setMarque(string marque)
        {
            _marque = marque;
        }
    
        public string marque
        {
            get { return _marque; }
            set { _marque = "peugeot"; }
        }
    
        public void couperContact()
        {
            _demarrer = false;
        }


    }
} 
        
        
    

