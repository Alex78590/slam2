﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionContact
   
    {
        [Serializable]
        public class Contact
        {
            public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }
        public string Prenom
        {
            get { return _prenom; }
            set { _prenom = value; }
        }
        public string Telephone
        {
            get { return _telephone; }
            set { _telephone = value; }
        }

        private string _telephone;
        private string _prenom;
        private string _nom;
        }
    }

