﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace GestionContact
{
    class FormMenuContact : Form
    {
        private Button buttonAjouterContact;
        private Button buttonListContact;
        public FormMenuContact() : base() 
        {
            buttonAjouterContact = new Button();

            this.buttonAjouterContact.Location = new System.Drawing.Point(20, 20);
            this.buttonAjouterContact.Size = new System.Drawing.Size(100, 30);
            this.buttonAjouterContact.Text = "Ajouter un contact";
            this.buttonAjouterContact.BackColor = Color.Aqua;

            this.Controls.Add(buttonAjouterContact);
            

            this.buttonAjouterContact.Click += btn_ajouterContact_Click;

            buttonListContact = new Button();

            this.buttonListContact.Location = new System.Drawing.Point(20, 60);
            this.buttonListContact.Size = new System.Drawing.Size(100, 30);
            this.buttonListContact.Text = "Liste contact";
            this.buttonListContact.BackColor = Color.Aqua;

            this.Controls.Add(buttonListContact);

            this.buttonListContact.Click += btn_ListContact_Click;

        }
        public void btn_ajouterContact_Click(object sender, EventArgs e)
        {
            FormAjoutContact fac = new FormAjoutContact();
            fac.Show();
        }

        public void btn_ListContact_Click(object sender, EventArgs e)
        {
            FormListContact fac = new FormListContact();
            fac.Show();
        }
        
        
           
        

    }
    

}
