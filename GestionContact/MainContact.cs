﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace GestionContact
{
    class MainContact
    {
        static private List<Contact> collectionContact;

        public static List<Contact> CollectionContact
        {
            get {return MainContact.collectionContact;}
        }

        static void Main (string[] args)
        {
            Application.ApplicationExit += Application_ApplicationExit;
            collectionContact = Persistance.ChargerListeContact();
            if (collectionContact == null)
            {
                collectionContact = new List<Contact>();
            }
            Application.Run(new FormMenuContact());
        }

        static void Application_ApplicationExit(object sender, EventArgs e)
        {
            Persistance.SauvegardeListeContact(collectionContact);
        }
        
    }
}
