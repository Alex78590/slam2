﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace GestionContact
{
    class FormAjoutContact : Form
    {
        private TextBox textBoxTelephone;
        private TextBox textBoxPrenom;
        private TextBox textBoxNom;
        private Button buttonValiderAjoutContact;
        private GroupBox groupBoxAjoutContact;

        public FormAjoutContact()
        {
            //controles de la fenetre
            this.groupBoxAjoutContact = new GroupBox();
            this.buttonValiderAjoutContact = new Button();
            this.textBoxNom = new TextBox();
            this.textBoxPrenom = new TextBox();
            this.textBoxTelephone = new TextBox();
            this.Controls.Add(this.groupBoxAjoutContact);

            //Les champs de saisis et le boutton sont ajoutés à la collection de controles de l'objet "GroupeBox"
            this.groupBoxAjoutContact.Controls.Add(this.textBoxTelephone);
            this.groupBoxAjoutContact.Controls.Add(this.textBoxPrenom);
            this.groupBoxAjoutContact.Controls.Add(this.textBoxNom);
            this.groupBoxAjoutContact.Controls.Add(this.buttonValiderAjoutContact);
            
            //Configuration de l'objet "GroupeBox"
            this.groupBoxAjoutContact.Location = new System.Drawing.Point(48, 33);
            this.groupBoxAjoutContact.Name = "groupBoxAjouterContact";
            this.groupBoxAjoutContact.Size = new System.Drawing.Size(305, 359);
            this.groupBoxAjoutContact.TabIndex = 0;
            this.groupBoxAjoutContact.TabStop = false;
            this.groupBoxAjoutContact.Text = "FORMULAIRE D'AJOUT D'UN CONTACT";

            //Valider un contact
            this.buttonValiderAjoutContact.Location = new System.Drawing.Point(163, 295);
            this.buttonValiderAjoutContact.Name = "button1";
            this.buttonValiderAjoutContact.Size = new System.Drawing.Size(100, 23);
            this.buttonValiderAjoutContact.TabIndex = 0;
            this.buttonValiderAjoutContact.Text = "AJOUTER";
            this.buttonValiderAjoutContact.UseVisualStyleBackColor = true;

            //configuration de l'objet "TextBoxNom"
            this.textBoxNom.Location = new System.Drawing.Point(43, 58);
            this.textBoxNom.Name = "Nom";
            this.textBoxNom.Size = new System.Drawing.Size(200, 20);
            this.textBoxNom.TabIndex = 1;
            this.textBoxNom.Text = "Saisir le nom du contact";

            //configuration de l'objet "TextBoxPrenom"
            this.textBoxPrenom.Location = new System.Drawing.Point(43, 103);
            this.textBoxPrenom.Name = "Prenom";
            this.textBoxPrenom.Size = new System.Drawing.Size(200, 20);
            this.textBoxPrenom.TabIndex = 2;
            this.textBoxPrenom.Text = "Saisir le prenom";

            //configuration de l'objet "TextBoxTelephone"
            this.textBoxTelephone.Location = new System.Drawing.Point(43, 152);
            this.textBoxTelephone.Name = "Telephone";
            this.textBoxTelephone.Size = new System.Drawing.Size(200, 20);
            this.textBoxTelephone.TabIndex = 3;
            this.textBoxTelephone.Text = "Saisir le telephone";

            //ClientSize
            this.ClientSize = new System.Drawing.Size(500, 500);
            this.Controls.Add(this.groupBoxAjoutContact);
            this.Name = "FormAjoutContact";
            
        }
        private void buttonValiderAjoutContact_Click(object sender, EventArgs e)
        {
            Contact c = new Contact();
            c.Nom = this.textBoxNom.Text;
            c.Prenom = this.textBoxPrenom.Text;
            c.Telephone = this.textBoxTelephone.Text;
            MainContact.CollectionContact.Add(c);
        }
       
    }

}
