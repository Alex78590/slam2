﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace GestionContact
{
    public static class Persistance
    {
        private static string repertoireApplication = Environment.CurrentDirectory + @"\";
        public static void SauvegardeListeContact(List<Contact> collectionContact)

        {
            FileStream file = null;
            file = File.Open(repertoireApplication + "contacts", FileMode.OpenOrCreate);
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(file, collectionContact);
            file.Close();
        }

        public static List<Contact> ChargerListeContact()
        {
            List<Contact> listeContact = null;
            FileStream fs = null;
            fs = new FileStream(repertoireApplication + "contacts", FileMode.OpenOrCreate);
            BinaryFormatter formatter = new BinaryFormatter();
            try
                {
                listeContact = formatter.Deserialize(fs) as List<Contact>;
            }
            catch (SerializationException se)
            {

            }
            fs.Close();
            return listeContact;
        }
    }
}

